#Install project
if echo "$1" | grep -iq "install" ;then
  eval "docker-compose up --build -d"
  eval "docker-compose exec nginx chown -R laravel:laravel var/www/html/api"
  if [ ! -f "${PWD%/*}/api/.env" ]; then
    echo "local" > ${PWD%/*}/api/.env
  fi
fi

#Rebulid project
if echo "$1" | grep -iq "rebuild" ;then
  echo "rebulid";
  eval "docker-compose up -d nginx"
fi


#Run container
if echo "$1" | grep -iq "start" ;then
  eval "docker-compose up -d nginx"
fi