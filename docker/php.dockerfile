# Use the official PHP image with FPM
FROM php:8.2-fpm

# Install PHP extensions installer
COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/
COPY --from=composer:2 /usr/bin/composer /usr/local/bin/composer

# Install PHP extensions
RUN install-php-extensions pdo pdo_pgsql pdo_mysql

# Install base extensions
RUN apt update && apt-get install -y \
    libzip-dev \
    zip \
    && docker-php-ext-install zip

# Set environment variables for user and group
ENV PHPGROUP=laravel
ENV PHPUSER=laravel

# Create user and group
RUN groupadd laravel && useradd -g laravel laravel

# Configure PHP-FPM to run as the new user
RUN sed -i "s/user = www-data/user = ${PHPUSER}/g" /usr/local/etc/php-fpm.d/www.conf && \
    sed -i "s/group = www-data/group = ${PHPGROUP}/g" /usr/local/etc/php-fpm.d/www.conf

# Create necessary directories and set permissions
RUN mkdir -p /var/www/html/api/public && \
    chown -R laravel:laravel /var/www/html/api

# Set environment variables for Composer
ENV COMPOSER_ALLOW_SUPERUSER=1

# Copy application files
COPY api/composer.json /var/www/html/api/
COPY api/composer.lock /var/www/html/api/
COPY api/.local.env /var/www/html/api/
COPY api/artisan /var/www/html/api/
COPY api/bootstrap/app.php /var/www/html/api/bootstrap/
COPY api/bootstrap/environment.php /var/www/html/api/bootstrap/
COPY api/app/Console /var/www/html/api/app/

# Install PHP dependencies
RUN cd /var/www/html/api && composer install --no-scripts

# Set the entrypoint for the container
CMD ["php-fpm", "-y", "/usr/local/etc/php-fpm.conf", "-R"]
