FROM nginx:stable-alpine

ENV NGINXUSER=laravel
ENV NGINXGROUP=laravel

RUN mkdir -p /var/www/html/api/public

#kopiuje domyślną konfiguracje nginx
ADD nginx-default.conf /etc/nginx/conf.d/default.conf

#sed <= edytor strumienia, tekstu itd 
# poniższa komenda oznacza: znajdź string "/user www..." i zamień www-data/user na laravel
RUN sed -i "s/user www-data/user ${NGINXUSER}}/g" /etc/nginx/nginx.conf

#dodaj użytkownika laravel z grupą laravel i shelem /bin/sh
RUN adduser -g ${NGINXGROUP} -s /bin/sh -D ${NGINXUSER}

RUN chown -R laravel:laravel /var/www/html/api