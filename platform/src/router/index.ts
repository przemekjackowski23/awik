import { createRouter, createWebHistory } from 'vue-router'
import LoginView from '../views/LoginView.vue'
import AdvertisementsView from '../views/AdvertisementsView.vue'
import AdvertisementView from '../views/AdvertisementView.vue'
import AdvertisementEditView from '../views/AdvertisementEditView.vue'
import RegistrationView from '../views/RegistrationView.vue'
import ConfigureStepsView from '../views/ConfigureStepsView.vue'
import AboutView from '../views/AboutView.vue';
import ProfileView from '../views/ProfileView.vue';
import { useAuthentication } from '../composables/useAuthentication';

const { isAuthenticated, getUser } = useAuthentication();

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/login',
      name: 'login',
      component: LoginView,
      meta: {
        isAuth: false,
      }
    },
    {
      path: '/register',
      name: 'registration',
      component: RegistrationView,
      meta: {
        isAuth: false,
      }
    },
    {
      path: '/about',
      name: 'about',
      component: AboutView,
      meta: {
        isAuth: false,
      }
    },
    {
      path: '/',
      name: 'advertises',
      component: AdvertisementsView,
      meta: {
        isAuth: false,
      }
    },
    {
      path: '/configure',
      name: 'configure',
      component: ConfigureStepsView,
      meta: {
        isAuth: true,
      }
    },
    {
      path: '/profile',
      name: 'profile',
      component: ProfileView,
      meta: {
        isAuth: true,
      }
    },
    {
      path: '/preview/:advertisement',
      name: 'previewAdvertisement',
      component: AdvertisementView,
      meta: {
        isAuth: false,
      }
    },
    {
      path: '/edit/:advertisement',
      name: 'editAdvertisement',
      component: AdvertisementEditView,
      meta: {
        isAuth: true,
      }
    },
  ]
});

router.beforeEach((to, from, next) => {
  getUser();
  if(to.meta.isAuth && !isAuthenticated.value) {
    if(from.name === 'login') return false;
    next('/');
  }
  next();
})

export default router
