import axios from 'axios';

const AxiosInstance = axios.create({
  baseURL: 'http://localhost:8000/api',
  headers: { 'X-Requested-With': 'XMLHttpRequest', 'Accept': 'application/json' },
  withCredentials: true,
  withXSRFToken: true,
});

export default AxiosInstance;
