export function hConvertImageToBase64(file = {}, callback = () => {}) {
  const reader = new FileReader();
  reader.addEventListener("load", () => callback(reader.result));
  reader.readAsDataURL(file);
}
