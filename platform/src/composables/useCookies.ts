export function useCookies() {
  const get = (name) => {
    return document.cookie.split(';').some(c => {
      return c.trim().startsWith(name + '=');
    });
  }

  const remove = (name) => {
    if (get(name)) {
      document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    }
  }

    return {
      get,
      remove
    }
  }