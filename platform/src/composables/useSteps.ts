import { ref } from 'vue';

const currentStep = ref(1)

export function useSteps(){
  const handleStepChange = (step: number): void => {
    currentStep.value = step;
    window.scrollTo(0, 0);
  };
  return {
    currentStep,
    handleStepChange
  }
}