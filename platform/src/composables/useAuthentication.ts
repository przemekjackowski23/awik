import AxiosInstance from '../config/axios';
import { useCookies } from './useCookies';
import { ref } from 'vue';
import { useRouter } from "vue-router";

const { remove } = useCookies();
const isAuthenticated = ref(false);
const userData = ref(null);

export function useAuthentication() {
  const router = useRouter();
  const csrf = async () => await AxiosInstance.get(`${import.meta.env.VITE_API_URL}/sanctum/csrf-cookie`);

  const login = async (formData: any) => {
    try {
      await csrf();
      await AxiosInstance.post('/login', { ...formData });
      return await getUser();
    } catch (error) {
      console.log(error);
    }
  }

  const getUser = async () => {
    try {
      const response = (await AxiosInstance.get('/user')).data;
      isAuthenticated.value = true;
      userData.value = response;
      return response;
    } catch (error) {
      isAuthenticated.value = false;
      userData.value = null;
      console.log(error);
    }
  }

  const register = async (formData) => {
    await csrf();
    await AxiosInstance.post('/register', { ...formData });
    router.push({ name: "configure" });
    return await getUser();
  }
  const logout = async () => {
    await AxiosInstance.post('/logout');
    isAuthenticated.value = false;
    router.push({ name: "advertises" });
    userData.value = null;
    remove('XSRF-TOKEN');
  }

  return {
    login,
    register,
    logout,
    getUser,
    isAuthenticated,
    userData
  }
}
