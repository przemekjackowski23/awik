import AxiosInstance from '@/config/axios'
import { ref } from 'vue'

const professions = ref([])
const isProfessionsLoading = ref(false)
const userPortfolio = ref({
  profession_id: null,
})
const userExperiences = ref([])
export function usePortfolio() {
  const getProfessions = async () => {
    try {
      isProfessionsLoading.value = true
      await AxiosInstance.get('/portfolio/getProfessions').then((response) => {
        professions.value = response.data
      })
    } catch (error) {
      console.log(error)
    } finally {
      isProfessionsLoading.value = false
    }
  }

  return {
    isProfessionsLoading,
    professions,
    getProfessions,
    userPortfolio,
    userExperiences,
  }
}
