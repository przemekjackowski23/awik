import AxiosInstance from '@/config/axios';
import { ref } from 'vue';

const usersExperiences = ref([]);
const isExperiencesLoading = ref(false);

export function useExperience() {

  const addExperience = async (experience: Object) => {
    try {
      isExperiencesLoading.value = true;
      const response = await AxiosInstance.post('/experience/save', experience);
      if (response.status == 200) usersExperiences.value = response.data;
      
    } catch (error) {
      console.log(error);
    } finally {
      isExperiencesLoading.value = false;
    }
  }

  const getUserExperience = async () => {
    try {
      isExperiencesLoading.value = true;
      await AxiosInstance.get('experience/list').then(response => {
        usersExperiences.value = response.data;
      })
    } catch (error) {
      console.log(error);
    } finally {
      isExperiencesLoading.value = false;
    }
  }
  return {
    usersExperiences,
    isExperiencesLoading,
    addExperience,
    getUserExperience
  }
}