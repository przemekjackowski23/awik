import AxiosInstance from '@/config/axios';
import { ref } from 'vue';

const advertisements = ref([]);
const cities = ref([]);
const isLoading = ref(false);

export function useAdvertisements() {

  const add = async (advertisement: Object) => {
    try {
      isLoading.value = true;
      await AxiosInstance.post('/advertisements/save', { ...advertisement }).then(response => {
        if (response.status == 200) advertisements.value = response.data;
      });
    } catch (error) {
      console.log(error);
    } finally {
      isLoading.value = false;
    }
  }

  const getList = async (isSpecifiedUser: Boolean) => {
    try {
      isLoading.value = true;
      await AxiosInstance.get(!isSpecifiedUser ? '/advertisements/list' : '/advertisements/user/list').then(response => {
        advertisements.value = response.data;
      })
    } catch (error) {
      console.log(error);
    } finally {
      isLoading.value = false;
    }
  }

  const getCities = async (query: String) => {
    if (!query) return;
    try {
      isLoading.value = true;
      const params = { query };
      await AxiosInstance.get('/cities/list', { params }).then(response => {
        cities.value = response.data;
      });
    } catch (error) {
      console.log(error);
    } finally {
      isLoading.value = false;
    }
  }

  const getDetails = async (uuid: String): Promise<Object> => {
    let previewData = advertisements.value.find(advertisement => advertisement.uuid === uuid);
    try {
      if (!previewData) {
        isLoading.value = true;
        const params = { uuid };
        await AxiosInstance.get('/advertisements/details', { params }).then(response => {
          previewData = response.data;
        });
      }
    } catch (error) {
      console.log(error);
    } finally {
      isLoading.value = false;
      return previewData;
    }
  }

  const clearList = () => {
    advertisements.value = [];
  }

  return {
    add,
    getList,
    getDetails,
    advertisements,
    getCities,
    isLoading,
    cities,
    clearList
  }
}