import AxiosInstance from '@/config/axios'
import { ref } from 'vue'

const isEducationLevelLoading = ref(false)
const isUserEducationLoading = ref(false)
const educationLevels = ref([
  { name: 'Wykształcenie podstawowe' },
  { name: 'Wykształcenie gimnazjalne' },
  { name: 'Wykształcenie zawodowe' },
  { name: 'Wykształcenie średnie' },
  { name: 'Wykształcenie wyższe - niepełne' },
  { name: 'Wykształcenie wyższe - pełne' }
])
const usersEducation = ref({})

export function useEducation() {
  const add = async (education: Object) => {
    try {
      isEducationLevelLoading.value = true
      await AxiosInstance.post('/education/save', { ...education }).then((response) => {
        if (response.status == 200) usersEducation.value = response.data
      })
    } catch (error) {
      console.log(error)
    } finally {
      isEducationLevelLoading.value = false
    }
  }
  const getUserEducation = async () => {
    try {
      isUserEducationLoading.value = true;
      await AxiosInstance.get('education/list').then(response => {
        usersEducation.value = response.data;
      })
    } catch (error) {
      console.log(error);
    } finally {
      isUserEducationLoading.value = false;
    }
  }
  const getEducationLevels = async () => {
    try {
      isEducationLevelLoading.value = true
      // await AxiosInstance.get('/educationLevels/list').then(response => {
      //   educationLevels.value = response.data;
      // });
    } catch (error) {
      console.log(error)
    } finally {
      isEducationLevelLoading.value = false
    }
  }

  return {
    getEducationLevels,
    isEducationLevelLoading,
    educationLevels,
    add,
    getUserEducation,
    isUserEducationLoading,
    usersEducation
  }
}
