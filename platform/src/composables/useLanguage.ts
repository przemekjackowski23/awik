import AxiosInstance from '@/config/axios';
import { ref } from 'vue';

const usersLanguages = ref([]);
const isLanguagesLoading = ref(false);

export function useLanguage() {

  const addLanguage = async (language: Object) => {
    try {
      isLanguagesLoading.value = true;
      const response = await AxiosInstance.post('/language/save', language);
      if (response.status == 200) usersLanguages.value = response.data;
      
    } catch (error) {
      console.log(error);
    } finally {
      isLanguagesLoading.value = false;
    }
  }

  const getUserLanguage = async () => {
    try {
      isLanguagesLoading.value = true;
      await AxiosInstance.get('language/list').then(response => {
        usersLanguages.value = response.data;
      })
    } catch (error) {
      console.log(error);
    } finally {
      isLanguagesLoading.value = false;
    }
  }
  return {
    usersLanguages,
    isLanguagesLoading,
    addLanguage,
    getUserLanguage
  }
}