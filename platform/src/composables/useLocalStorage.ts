export function useLocalStorage(){
  const save = (key, value) => {
    return localStorage.setItem(key, value);  
  }
  const remove = (key) => {
    return localStorage.removeItem(key);
  }
  const get = (key) => {
    return localStorage.getItem(key);
  }

  return {
    save,
    remove,
    get
  }
}