<h1>AWIK</h1>

W celu uruchomienia projektu lokalnie należy mieć zainstalowany Docker Desktop oraz uruchomić polecenie:
```bash
sh cli.sh install
```

W przypadku pierwszego uruchomienia należy wejść do kontenera PHP i uruchomić polecenia:
```bash
cd api, composer install
```
(plik znajduje się w folderze /docker).

Warstwa front-end dziala pod adresem http://localhost:3004.<br>
Warstwa back-end działa pod adresem http://localhost:8000.

Bazę danych należy uruchomić lokalnie, zalecane użycie PgAdmin 4.<br>
Do zarządzania bazą zalecany DBeaver Community Edition.

Wersja node: 20.10.0.
Wersja php: 8.2.
Baza danych: PgSQL.

<p>Testy należy uruchomić poleceniem</p>

```bash
composer test-that
```
w terminalu kontenera PHP.
<p><b>Nie używać php artisan test!!!<b></p>