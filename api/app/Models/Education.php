<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use App\Models\{User, EducationLevel};


class Education extends Model
{
  use HasApiTokens, HasFactory, Notifiable;
  protected $table = 'educations';
  /**
   * The attributes that are mass assignable.
   *
   * @var array<int, string>
   */
  protected $fillable = [
    'fk_user_id',
    'uuid',
    'school',
    'field',
    'degree',
    'date_start',
    'date_end'
  ];

  /**
   * Get the user associated with the education.
   */
  public function users(): HasOne
  {
    return $this->hasOne(User::class,'id','fk_user_id');
  }
}
