<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use App\Models\User;


class Experience extends Model
{
  use HasApiTokens, HasFactory, Notifiable;

  /**
   * The attributes that are mass assignable.
   *
   * @var array<int, string>
   */
  protected $fillable = [
    'fk_user_id',
    'position',
    'uuid',
    'start_date',
    'end_date',
    'company',
    'fk_city_id'
  ];

  /**
   * Get the user associated with the advertisement.
   */
  public function users(): HasOne
  {
    return $this->hasOne(User::class,'id','fk_user_id');
  }
}
