<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\City;

class CitiesController extends Controller
{
    public function list(Request $request): Response{
        $request->validate([
            'query' => ['required', 'string', 'max:50'],
        ]);
        $cities = City::where('city','like', '%'.$request->input('query').'%')->get();
        return response($cities, 200);
    }
}
