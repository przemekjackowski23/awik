<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\{Profession};

class PortfolioController extends Controller
{
    public function getProfessions(Request $request): Response{
        $professions = Profession::all();
        return response($professions, 200);
    }
}
