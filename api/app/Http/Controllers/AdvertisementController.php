<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\{Advertisement, City};

class AdvertisementController extends Controller
{
    /**
     * Handle an incoming registration request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function save(Request $request): Response
    {
        $request->validate([
            'city' => ['required', 'string', 'max:255'],
            'country' => ['required', 'string', 'max:255'],
            'type' => ['required', 'string', 'max:255'],
            'category' => ['required', 'string', 'max:255'],
            'profession' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string', 'max:255'],
            'salary' => ['required', 'string', 'max:255'],
            'requirements' => ['required', 'string', 'max:255'],
        ]);

        $advertisement = Advertisement::create([
            'city' => $request->city,
            'country' => $request->country,
            'type' => $request->type,
            'category' => $request->category,
            'profession' => $request->profession,
            'description' => $request->description,
            'salary' => $request->salary,
            'requirements' => $request->requirements,
            'fk_user_id' => $request->user()->id
        ]);

        $advertisement = $advertisement->fresh()->with('users')->get();
        return response($advertisement, 200);
    }

    public function list(Request $request): Response
    {
        $advertisements = Advertisement::with('users')->get();
        return response($advertisements, 200);
    }

    public function details(Request $request): Response
    {
        $advertisements = Advertisement::with('users')->where('uuid', $request->input('uuid'))->get()->first();
        return response($advertisements, 200);
    }
    
    public function userList(Request $request): Response
    {
        $advertisements = Advertisement::with('users')->where('fk_user_id', $request->user()->id)->get();
        return response($advertisements, 200);
    }
}
