<?php
/* Created for change route prefix - /api/sanctum/csrf-cookie */
namespace App\Http\Controllers\Auth;

use Laravel\Sanctum\Http\Controllers\CsrfCookieController;

class CsrfController extends CsrfCookieController {}