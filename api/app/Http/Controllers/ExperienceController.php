<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Experience;
use Illuminate\Support\Str;
use Carbon\Carbon;

class ExperienceController extends Controller
{
    public function save(Request $request): Response
    {
        // $request->validate([
        //     'name' => ['required', 'string', 'max:100'],
        //     'start_date' => ['required', 'string', 'max:100'],
        //     'end_date' => ['required', 'string', 'max:100'],
        //     'company' => ['required', 'string', 'max:100'],
        //     'location' => ['required', 'string', 'max:100'],
        // ]);

        $experience = Experience::create([
            'uuid' => (string) Str::uuid(),
            'position' => $request->get('name'),
            'start_date' => Carbon::parse($request->get('startDate')),
            'end_date' => Carbon::parse($request->get('endDate')),
            'company' => $request->get('company'),
            'fk_city_id' => $request->get('location'),
            'fk_user_id' => $request->user()->id
        ]);

        $experience = $experience->fresh()->with('users')->get();
        return response($experience, 200);
    }

    public function list(Request $request): Response
    {
        $experiences = Experience::where('fk_user_id', $request->user()->id)->get();
        return response($experiences, 200);
    }

}
