<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Language;

class LanguageController extends Controller
{
    public function list(Request $request): Response
    {
        $levels = Language::all();
        return response($levels, 200);
    }

    public function save(Request $request): Response
    {
        $lang = Language::create([
            'name' => $request->get('name'),
            'fk_user_id' => $request->user()->id
        ]);

        return response($lang, 200);
    }
}
