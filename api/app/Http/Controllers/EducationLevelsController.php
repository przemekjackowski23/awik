<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\EducationLevel;

class EducationLevelsController extends Controller
{
    public function list(Request $request): Response
    {
        $levels = EducationLevel::all();
        return response($levels, 200);
    }
}
