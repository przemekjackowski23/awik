<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Education;
use Illuminate\Support\Str;
use Carbon\Carbon;

class EducationController extends Controller
{
    /**
     * Handle an incoming registration request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function save(Request $request): Response
    {
        $education = Education::create([
            'uuid' => (string) Str::uuid(),
            'school' => $request->get('school'),
            'field' => $request->get('field'),
            'fk_user_id' => $request->user()->id,
            'degree' => $request->get('degree'),
            'date_start' => Carbon::parse($request->get('date_start')),
            'date_end' => Carbon::parse($request->get('date_end')),
        ]);

        return response($education, 200);
    }

    /**
     * Handle an incoming registration request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function list(Request $request): Response
    {
        $educations = Education::with('users')->get();
        return response($educations, 200);
    }
}
