<?php
namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Advertisement;

class AdvertisementTest extends TestCase
{
  /** @test */
  public function the_posts_show_route_can_be_accessed()
  {
    // Arrange
    // Dodajmy do bazy danych wpis
    $post = Advertisement::create([
      'title' => 'Wrabiał krowę w morderstwo cioci',
    ]);

    // Act
    // Wykonajmy zapytanie pod adres wpisu
    $response = $this->get('/posts/' . $post->id);

    // Assert
    // Sprawdźmy że w odpowiedzi znajduje się tytuł wpisu
    $response->assertStatus(200)
      ->assertSeeText('Wrabiał krowę w morderstwo cioci');
  }
}
