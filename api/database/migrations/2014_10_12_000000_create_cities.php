<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Ramsey\Uuid\Uuid;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->uuid('uuid')->primary()->default(Uuid::uuid4());
            $table->string('city');
            $table->double('lat');
            $table->double('lng');
            $table->string('country');
            $table->string('admin_name');
            $table->string('iso2');
            $table->string('capital');
            $table->bigInteger('population');
            $table->bigInteger('population_proper');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cities');
    }
};
