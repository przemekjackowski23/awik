<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Ramsey\Uuid\Uuid;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('licenses', function (Blueprint $table) {
            $table->uuid('uuid')->primary()->default(Uuid::uuid4());
            $table->uuid('fk_user_uuid');
            $table->string('college');
            $table->string('qualification');
            $table->timestamp('date_start');
            $table->timestamp('date_end');
            $table->timestamps();
            $table->foreign('fk_user_uuid')->references('uuid')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('licenses');
    }
};
