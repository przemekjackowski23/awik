<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{
    AdvertisementController,
    PortfolioController,
    ExperienceController,
    EducationController,
    CitiesController,
    EducationLevelsController,
    LanguageController
};

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });
    //advertisements
    Route::post('/advertisements/save', [AdvertisementController::class, 'save']);
    Route::get('/advertisements/user/list', [AdvertisementController::class, 'userList']);

    //cities
    Route::get('/cities/list', [CitiesController::class, 'list']);

    //portfolio
    Route::get('/portfolio/getProfessions', [PortfolioController::class, 'getProfessions']);

    //experiences
    Route::post('experience/save', [ExperienceController::class, 'save']);
    Route::get('experience/list', [ExperienceController::class, 'list']);

    //education
    Route::get('educationLevels/list', [EducationLevelsController::class, 'list']);
    Route::post('education/save', [EducationController::class, 'save']);
    Route::get('education/list', [EducationController::class, 'list']);

    //languages
    Route::get('language/list', [LanguageController::class, 'list']);
    Route::post('language/save', [LanguageController::class, 'save']);
});
//advertisements
Route::get('/advertisements/list', [AdvertisementController::class, 'list']);
Route::get('/advertisements/details', [AdvertisementController::class, 'details']);

